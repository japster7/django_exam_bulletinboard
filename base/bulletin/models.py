from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

user_roles = [
    "Poster",
    "Moderator",
    "Administrator",
]
user_role_selection = [(i+1, user_roles[i]) for i in range(len(user_roles))]


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.IntegerField(default=1, choices=user_role_selection)
    is_banned = models.BooleanField(default=False)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.user.username)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.slug


class Board(models.Model):
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(Person, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    position = models.IntegerField(default=0)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def get_thread_with_latest_reply(self):
        threads = self.thread_set.all()
        count = threads.count()
        if count > 0:
            latest_date, index = threads[0].get_latest_reply_date(), 0
            for i in range(1, threads.count()):
                curr_date = threads[i].get_latest_reply_date()
                if not latest_date or curr_date and curr_date > latest_date:
                    latest_date = curr_date
                    index = i
            return threads[index]

    def post_count(self):
        threads = self.thread_set.all()
        count = 0
        for thread in threads:
            count += thread.post_set.count()
        return count


class Thread(models.Model):
    title = models.CharField(max_length=100)
    posted_by = models.ForeignKey(Person, on_delete=models.CASCADE)
    posted_on = models.DateTimeField(auto_now_add=True)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    is_locked = models.BooleanField(default=False)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.slug

    def get_latest_reply(self):
        if self.post_set.count() > 0:
            return self.post_set.order_by('-posted_on')[0]

    def get_latest_reply_date(self):
        reply = self.get_latest_reply()
        if reply:
            return reply.posted_on


class Post(models.Model):
    message = models.TextField()
    posted_by = models.ForeignKey(Person, on_delete=models.CASCADE)
    posted_on = models.DateTimeField(auto_now_add=True)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)

    def __str__(self):
        return self.message


User._meta.get_field('last_name').blank = False
User._meta.get_field('last_name').null = False
User._meta.get_field('first_name').blank = False
User._meta.get_field('first_name').null = False
