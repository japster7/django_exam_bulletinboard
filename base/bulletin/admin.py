from django.contrib import admin

from bulletin import models as bm

admin.site.register(bm.Person)
admin.site.register(bm.Board)
admin.site.register(bm.Thread)
admin.site.register(bm.Post)
