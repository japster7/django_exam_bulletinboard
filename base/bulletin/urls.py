from django.urls import path
from django.contrib.auth import views as av

from bulletin import views as bv

app_name = 'bulletin'
urlpatterns = [
    path('', bv.IndexView.as_view(), name='index'),
    path(
        'login/',
        av.LoginView.as_view(
            template_name='bulletin/login.html',
            extra_context={'no_nav': True},
            redirect_authenticated_user=True,
        ),
        name='login'
    ),
    path('logout/', av.LogoutView.as_view(), name='logout'),
    path(
        'password/change/',
        bv.PasswordUpdateView.as_view(),
        name='change_password'
    ),
    path(
        'register/',
        bv.UserCreateView.as_view(),
        name='register'
    ),
    path('board/add/', bv.AddBoardView.as_view(), name='add_board'),
    path(
        'board/<slug:slug>/thread/add/',
        bv.AddThreadView.as_view(),
        name='add_thread'
    ),
    path('board/<slug:slug>/', bv.BoardView.as_view(), name='board'),
    path('thread/<slug:slug>/', bv.ThreadView.as_view(), name='thread'),
    path(
        'thread/<slug:slug>/change_status/',
        bv.change_thread_status,
        name='change_thread_status'
    ),
    path('thread/<slug:slug>/post/add/', bv.PostView.as_view(), name='post'),
    path('<slug:slug>/', bv.ProfileView.as_view(), name='profile'),
    path(
        '<slug:slug>/change_person_status/',
        bv.change_person_status,
        name='change_person_status'
    ),
    path(
        '<slug:slug>/change_role/<int:role>/',
        bv.change_role,
        name='change_role'
    ),
]
