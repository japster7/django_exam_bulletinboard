from django.contrib.auth import models as am, forms as af

from bulletin import models as bm


class RegisterForm(af.UserCreationForm):
    class Meta:
        model = am.User
        fields = ('username', 'last_name', 'first_name', 'email')

    def save(self):
        user = super().save()
        bm.Person.objects.create(user=user)
        return user
