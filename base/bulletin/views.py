from django.contrib import messages as msg
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView

from bulletin import models as bm, forms as bf


class PasswordUpdateView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'bulletin/form.html'
    success_url = reverse_lazy('bulletin:index')
    success_message = 'Password Changed!'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Change Password'
        return context


class UserCreateView(SuccessMessageMixin, CreateView):
    template_name = 'bulletin/form.html'
    success_url = reverse_lazy('bulletin:index')
    success_message = 'User Created!'
    form_class = bf.RegisterForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create User'
        return context


class IndexView(ListView):
    context_object_name = 'boards'
    template_name = 'bulletin/index.html'
    paginate_by = 20

    def get_queryset(self):
        return bm.Board.objects.all().order_by('-position')


class BoardView(SingleObjectMixin, ListView):
    template_name = 'bulletin/board.html'
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=bm.Board.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["board"] = self.object
        return context

    def get_queryset(self):
        return self.object.thread_set.order_by('-posted_on')


class ThreadView(SingleObjectMixin, ListView):
    template_name = 'bulletin/thread.html'
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=bm.Thread.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["thread"] = self.object
        return context

    def get_queryset(self):
        return self.object.post_set.order_by('posted_on')


def role_test(self, level):
    if self.person.role >= bm.user_roles.index(level) + 1:
        return True
    raise Http404()


def administrator_test(self):
    return role_test(self, "Administrator")


def moderator_test(self):
    return role_test(self, "Moderator")


class AdministratorTestMixin(UserPassesTestMixin):
    def test_func(self):
        return administrator_test(self.request.user)


class ModeratorTestMixin(UserPassesTestMixin):
    def test_func(self):
        return moderator_test(self.request.user)


class AddBoardView(AdministratorTestMixin, CreateView):
    template_name = 'bulletin/form.html'
    success_url = reverse_lazy('bulletin:index')
    model = bm.Board
    fields = ('name', 'position')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Add Board'
        return context

    def form_valid(self, form):
        board = form.save(commit=False)
        board.creator = self.request.user.person
        try:
            board.save()
            msg.info(self.request, 'Board added!')
        except IntegrityError:
            msg.error(
                self.request,
                'Board cannot be created due to name conflict!'
            )
            return redirect(self.get_success_url())
        return super().form_valid(form)


class AddThreadView(ModeratorTestMixin, CreateView):
    template_name = 'bulletin/form.html'
    model = bm.Thread
    fields = ('title',)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Add Thread'
        return context

    def form_valid(self, form):
        thread = form.save(commit=False)
        thread.posted_by = self.request.user.person
        thread.board = bm.Board.objects.get(slug=self.kwargs['slug'])
        try:
            thread.save()
            msg.info(self.request, 'Thread added!')
        except IntegrityError:
            msg.error(
                self.request,
                'Thread cannot be created due to title conflict!'
            )
            return redirect(self.get_success_url())
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('bulletin:board', args=(self.kwargs['slug'],))


class PostView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'bulletin/form.html'
    success_message = 'Post Added!'
    model = bm.Post
    fields = ('message',)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Add Post'
        return context

    def form_valid(self, form):
        post = form.save(commit=False)
        post.posted_by = self.request.user.person
        post.thread = bm.Thread.objects.get(slug=self.kwargs['slug'])
        post.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('bulletin:thread', args=(self.kwargs['slug'],))


class ProfileView(SingleObjectMixin, ListView):
    model = bm.Person
    template_name = 'bulletin/profile.html'
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=bm.Person.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["person"] = self.object
        return context

    def get_queryset(self):
        return self.object.post_set.order_by('-posted_on')


@login_required
@user_passes_test(moderator_test)
def change_thread_status(request, slug):
    thread = bm.Thread.objects.get(slug=slug)
    thread.is_locked = not thread.is_locked
    if thread.is_locked:
        msg.error(request, 'Thread locked!')
    else:
        msg.info(request, 'Thread unlocked!')
    thread.save()
    return redirect(reverse('bulletin:thread', args=(slug,)))


@login_required
@user_passes_test(moderator_test)
def change_person_status(request, slug):
    person = bm.Person.objects.get(slug=slug)
    person.is_banned = not person.is_banned
    if person.is_banned:
        msg.error(request, 'Person banned!')
    else:
        msg.info(request, 'Person unbanned!')
    person.save()
    return redirect(reverse('bulletin:profile', args=(slug,)))


@login_required
@user_passes_test(administrator_test)
def change_role(request, slug, role):
    person = bm.Person.objects.get(slug=slug)
    person.role = role
    person.save()
    msg.warning(request, 'User role changed!')
    return redirect(reverse('bulletin:profile', args=(slug,)))
